import React from 'react';
import logo from './logo.svg';
import './App.css';
import { BrowserRouter, NavLink, Route, Routes } from 'react-router-dom';
import AppRoute from './AppRoute';

function App() {
  return (
    <BrowserRouter basename={process.env.PUBLIC_URL}>
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <p>
            Edit <code>src/App.tsx</code> and save to reload.
          </p>
          <a
            className="App-link"
            href="https://reactjs.org"
            target="_blank"
            rel="noopener noreferrer"
          >
            Learn React
          </a>
          <ul className='route'>
            { [1,2,3].map((item: number, index: number) => <NavLink key={`link-${index}`} to={`/route${item}`}>route{item}</NavLink>) }
          </ul>

          <Routes>
            { [1,2,3].map((item: number, index: number) => <Route key={`route-${index}`} path={`/route${item}`} element={<AppRoute />} />) }
          </Routes>
        </header>
      </div>
    </BrowserRouter>
  );
}

export default App;
